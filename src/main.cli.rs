extern crate image;

mod lib;
mod cli;

use std::env;
use std::process;
use image::{ImageBuffer, Rgb};
use crate::lib::{mandelbrot_parallel, DrawMandelbrot};

fn main() {
    let mut size: isize = 640;
    let mut max_iters: usize = 1000;

    // parse and collect arguments
    let args = env::args().zip(env::args().skip(1));
    for (key, val) in args {
        if key == "--size" {
            size = val.parse::<isize>().unwrap();
        }
        if key == "--max-iters" {
            max_iters = val.parse::<usize>().unwrap();
        }
        if key == "--help" || val == "--help" {
            println!("Usage: rust-mandelbrot-cli [--size <pixels>] [--max-iters <max mandelbrot iterations>] [--help]");
            process::exit(0);
        }
    }

    // a default (black) image containing Rgb values
    let mut image = ImageBuffer::<Rgb<u8>, Vec<u8>>::new(size as u32, size as u32);
    let mb = mandelbrot_parallel(0, 0, size, size, max_iters);
    image.draw_mandelbrot(&mb, max_iters, size as usize, size as usize);
    image.save("mandelbrot.png").unwrap();
}

extern crate image;

use image::{ImageBuffer, Pixel, Rgb};
use crate::lib::{colorize, DrawMandelbrot};

impl DrawMandelbrot for ImageBuffer<Rgb<u8>, Vec<u8>> {
    fn draw_mandelbrot(&mut self, set: &[usize], max_iters: usize, width: usize, _: usize) {
        for (idx, color) in colorize(&set, max_iters).enumerate() {
            let col = (idx % width) as u32;
            let row = (idx / width) as u32;

            self.put_pixel(col, row, *Rgb::from_slice(&[color[0], color[1], color[2]]));
        }
    }
}

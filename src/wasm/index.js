// Drawing to canvas can also be moved to WebAssembly:
// https://rustwasm.github.io/docs/wasm-bindgen/examples/2d-canvas.html

import { mandelbrot_naive, colorize_js, default as init } from './libmandelbrot.js';
var mandelbrot = { "x": 0, "y": 0, "zoom": 0 };

async function run() {
  await init();

  function drawMandelbrot() {
    var canvas = document.getElementsByTagName("canvas")[0];
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    var iters = parseInt(document.getElementById("iterationsTxt").value);
    if (isNaN(iters)) iters = 10;

    var mandelbrot_set = mandelbrot_naive(
      mandelbrot.x, mandelbrot.y, canvas.width, canvas.height, iters, mandelbrot.zoom
    );
    var mandelbrot_set = colorize_js(mandelbrot_set, iters);
    var context = canvas.getContext("2d");
    var image = context.getImageData(0, 0, canvas.width, canvas.height);
    var buffer = image.data;

    for (var idx = 0; idx < mandelbrot_set.length && idx < buffer.length; idx += 4) {
      buffer[idx + 0] = mandelbrot_set[idx + 0];
      buffer[idx + 1] = mandelbrot_set[idx + 1];
      buffer[idx + 2] = mandelbrot_set[idx + 2];
      buffer[idx + 3] = 255;
    }

    context.putImageData(image, 0, 0);
  }

  function center(e) {
    var bounds = e.target.getBoundingClientRect();

    var halfWidth = Math.round((bounds.right - bounds.left) / 2);
    var halfHeight = Math.round((bounds.bottom - bounds.top) / 2);
    var relX = Math.round(e.clientX - bounds.left) - halfWidth;
    var relY = Math.round(e.clientY - bounds.top) - halfHeight;

    mandelbrot.x += relX;
    mandelbrot.y += relY;
  }

  function zoom(e) {
    // Intention: Zoom when the right mouse button is clicked. Whether we zoom out
    // or in depends on whether or not the shift key is pressed. Unfortunately
    // doens't work for the shift-key case.
    mandelbrot.zoom += e.shiftKey ? -1 : 1;
  }

  var canvas = document.getElementsByTagName("canvas")[0];
  var draw = () => drawMandelbrot();
  var centerAndDraw = e => { center(e); draw() };
  //var zoomAndDraw = e => { e.preventDefault(); zoom(e); draw(); return false; };
  var zoomInAndDraw = () => { mandelbrot.zoom -= 1; draw(); }
  var zoomOutAndDraw = () => { mandelbrot.zoom += 1; draw(); }

  window.addEventListener("resize", draw, false);
  document.getElementById("redrawBtn").addEventListener("click", draw);
  document.getElementById("zoomInBtn").addEventListener("click", zoomInAndDraw);
  document.getElementById("zoomOutBtn").addEventListener("click", zoomOutAndDraw);
  canvas.addEventListener("click", centerAndDraw);
  //canvas.addEventListener("contextmenu", zoomAndDraw);

  drawMandelbrot();
}

run();

# Web frontend to rust-mandelbrot
To install for development purposes:

1. Install wasm pack: `cargo install wasm-pack`
2. From the repo's root directory, build the wasm lib: `wasm-pack build --target web`
3. Enter the wasm dir and ensure the below files point to the correct targets:
    1. `libmandelbrot.js` -> `../../pkg/libmandelbrot.js`
    2. `libmandelbrot_bg.wasm` -> `../../pkg/libmandelbrot_bg.wasm`
4. Install a webserver for dev purposes: `npm install http-server` (or any other will do)
5. Run the web server: `node_modules/.bin/http-server`
6. Check it out at [localhost:8080](http://localhost:8080).

To serve modifications, rebuild with `wasm-pack build --target web` in the repo root and reload your browser.

# To deploy
1. Copy the files in in this directory to your server
2. Replace the `libmandelbrot*` files with their counterparts in `../../pkg`

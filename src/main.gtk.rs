extern crate gtk;
extern crate gio;

use gio::prelude::*;

mod lib;
mod gtkui;


fn main() {
    gtk::init().unwrap();
    let settings = gtk::Settings::get_default().unwrap();
    let _ = settings.set_property("gtk-application-prefer-dark-theme", &true);

    // Create application state: app, window,
    let app = gtk::Application::new(
        Some("com.sobolt.Mandelbrot"),
        gio::ApplicationFlags::FLAGS_NONE
    ).unwrap();

    // Run the application, then draw the initial mandelbrot figure
    // gtkui::draw_mandelbrot_on_resize(&mut state.canvas.clone());
    let _state = gtkui::RustMandelbrotGtk::new(&app);
    let ret = app.run(&std::env::args().collect::<Vec<_>>());
    std::process::exit(ret);
}

use wasm_bindgen::prelude::*;
use js_sys::{Uint8Array, Array};
use rayon::prelude::*;


/// Calculate a mandelbrot fractal set in the naive fashion (i.e. per array value, without smart
/// tricks or optimizations).
///
/// # Arguments
/// `max_x` - The width (in pixels) to give the mandelbrot fractal
/// `max_y` - The height (in pixels) to give the mandelbrot fractal.
///
/// # Returns
/// A vector of values indicating whether or not a point is in the mandelbrot set or not
#[allow(dead_code)]
#[wasm_bindgen]
pub fn mandelbrot_naive(
    center_x: isize, center_y: isize, max_x: isize, max_y: isize, max_iters: usize, zoom: f64
) -> Vec<usize> {
    // Define the base mandelbrot coordinate system (MCS) over the y-axis
    let im_min = -2.0;
    let im_max = 2.0;

    // Calculate the size of each pixel in the MCS
    // The MCS' x-axis' size is determined by max_x, thus making the function compatible with
    // arbitrary rectangles
    let im_factor = (im_max - im_min) / max_y as f64;
    let re_min = (max_x / 2) as f64 * -im_factor;
    let re_max = (max_x / 2) as f64 * im_factor;

    let zoom = if zoom == 0.0 {1.0} else {zoom};
    let im_factor = im_factor / zoom;
    let re_factor = (re_max - re_min) / max_x as f64 / zoom;

    // Calculate the mandelbrot set
    let mut results = Vec::with_capacity((max_x * max_y) as usize);
    for y in center_y .. max_y + center_y {
        let im = (y as f64 - max_y as f64 * 0.5) * im_factor;
        for x in center_x .. max_x + center_x {
            let re = (x as f64 - max_x as f64 * 0.5) * re_factor;
            let cx = (re, im);

            let iters = mandelbrot_iters(cx, max_iters);
            results.push(iters);
        }
    }

    return results;
}

/// Calculate a mandelbrot fractal set with the naive algorithm, but parallellized.
///
/// # Arguments
/// `max_x` - The width (in pixels) to give the mandelbrot fractal
/// `max_y` - The height (in pixels) to give the mandelbrot fractal.
///
/// # Returns
/// A vector of values indicating whether or not a point is in the mandelbrot set or not
#[allow(dead_code)]
#[wasm_bindgen]
pub fn mandelbrot_parallel(
    center_x: isize, center_y: isize, max_x: isize, max_y: isize, max_iters: usize, zoom: f64,
) -> Vec<usize> {
    // Define the base mandelbrot coordinate system (MCS) over the y-axis
    let im_min = -2.0;
    let im_max = 2.0;

    // Calculate the size of each pixel in the MCS
    // The MCS' x-axis' size is determined by max_x, thus making the function compatible with
    // arbitrary rectangles
    let im_factor = (im_max - im_min) / max_y as f64;
    let re_min = (max_x / 2) as f64 * -im_factor;
    let re_max = (max_x / 2) as f64 * im_factor;
    let im_factor = im_factor  * 2f64.powf(-zoom);
    let re_factor = (re_max - re_min) / max_x as f64 * 2f64.powf(-zoom);

    // Define pixel coordinates to calculate the mandelbrot set for
    let ys = center_y .. max_y + center_y;
    let xs = center_x .. max_x + center_x;
    let cross: Vec<_> = ys.flat_map(|y| xs.clone().map(move |x| (y, x))).collect();

    // Calculate the mandelbrot set
    let results = cross.into_par_iter().map(|(y, x)| {
        let im = (y as f64 - max_y as f64 * 0.5) * im_factor;
        let re = (x as f64 - max_x as f64 * 0.5) * re_factor;
        let cx = (re, im);
        mandelbrot_iters(cx, max_iters)
    }).collect();

    return results;
}

/// JavaScript-specific function to take an arbitrarily-sized slice of integers and map them to
/// RGBA values, where max_iters was the amount of iterations.
///
/// # Arguments
/// `set` - A collection (slice) of iterations numbers. All values should be smaller than or
///     equal to `max_iters`.
/// `max_iters` - The maximum numbers of iterations with which `set` was calculated. No value in
///     in it should be greater than this parameter.
///
/// # Returns
/// A flat, 1D JavaScript compatible `Uint8Array` of colors (RGBA). Four successive values form the
/// R, G, B, and A (opacity) values of a single pixel.
#[allow(dead_code)]
#[wasm_bindgen]
pub fn colorize_js(set: &[usize], max_iters: usize) -> Uint8Array {
    // Colorize the given set and then flatten the result collection of [u8; 4] into an array of
    // JsValue<u8>
    let colors: Array = colorize(set, max_iters)
        .fold(Array::new(), |arr, rgba| {
            for v in rgba.iter() { arr.push(&JsValue::from(*v)); }
            arr
        });
    return Uint8Array::new(&colors);  // Turn the generic array into an array of bytes
}

/// Take an arbitrarily-sized slice of integers and map them to RGBA values, where max_iters was
/// the amount of iterations.
///
/// # Arguments
/// `set` - A collection (slice) of iterations numbers. All values should be smaller than or
///     equal to `max_iters`.
/// `max_iters` - The maximum numbers of iterations with which `set` was calculated. No value in
///     in it should be greater than this parameter.
///
/// # Returns
/// A 1D iterator of colors (RGBA, array of four bytes), one for each value in `set`.
pub fn colorize<'a>(set: &'a [usize], max_iters: usize) -> impl Iterator<Item = [u8; 4]> + 'a {
    // 1. Take the iterator of the collection of calculation iterations for each pixel
    // 2. Map over them all with a function that for each value:
    //      a. Converts the iterations and max iterations from usize to to float
    //      b. Calculates color values
    //      c. Gives an array of bytes/u8s that is four items long, one for R, G, B and A
    // 3. Return the collection of arrays
    return set.iter().map(move |iters| {
        match iters >= &max_iters {
            true => [0, 0, 0, 255],  // RGBA black
            false => {
                let factor = (*iters as f32 / max_iters as f32).sqrt();
                let value = (255.0 * factor) as u8;
                [value, value, value, 255]  // RGBA
            }
        }
    });
}

/// Given a complex number and max iteration depth, calculate how many iterations it takes to
/// determine whether or not this number falls within or without the mandelbrot set.
///
/// # Arguments
/// * `cx` - Two floats that together form the complex number of which is calculated whether it is
///     in the mandelbrot set or not.
/// * `max_iter` - How many iterations we want to do at most.
///
/// # Returns
/// The amount of iterations it took to decide, for which holds 0 <= return value <= max_iter.
fn mandelbrot_iters(cx: (f64, f64), max_iter: usize) -> usize {
    let mut x = 0.0;
    let mut y = 0.0;
    let mut n = 0;
    let (re, im) = cx;

    while x * x + y * y <= 4.0 && n < max_iter {
        let x_new = x * x - y * y + re;
        y = 2.0 * x * y + im;
        x = x_new;
        n += 1
    }

    return n;
}

/// Trait specifying the interface for drawing a mandelbrot result set.
///
/// This way, various 'backends' (HTML canvas, GDK pixbuf, ...), can be supported.
pub trait DrawMandelbrot {
    /// Draw a mandelbrot fractal.
    ///
    /// # Arguments
    /// * `self` - The reference to the struct that will draw the fractal.
    /// * `set` - The set of iterations.
    /// * `max_iters` - The maximum number of iterations set used to calculate `set`.
    /// * `width` - The image's desired width.
    /// * `height` - The image's desired height.
    fn draw_mandelbrot(&mut self, set: &[usize], max_iters: usize, width: usize, height: usize);
}


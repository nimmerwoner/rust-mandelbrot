use gtk;

use std::cell::RefCell;
use std::rc::Rc;

use gtk::prelude::*;
use gio::prelude::*;

use crate::lib::{mandelbrot_parallel, colorize, DrawMandelbrot};

pub struct RustMandelbrotGtk {
    window: gtk::Window,
    scrolled_window: gtk::ScrolledWindow,
    canvas: gtk::Image,
    canvas_events: gtk::EventBox,
    redraw: gtk::Button,
    iters: gtk::Entry,
    zoom_out: gtk::Button,
    zoom_fit: gtk::Button,
    zoom_in: gtk::Button,
    offset_x: isize,
    offset_y: isize,
    zoom: f64,
}

impl RustMandelbrotGtk {
    pub fn new(application: &gtk::Application) -> Rc<RefCell<RustMandelbrotGtk>> {
        let builder = gtk::Builder::from_file("src/gtkui/window.glade");
        builder.set_application(application);

        let state = RustMandelbrotGtk {
            window: builder.get_object("window").unwrap(),
            scrolled_window: builder.get_object("scrolled_window").unwrap(),
            canvas: builder.get_object("canvas").unwrap(),
            canvas_events: builder.get_object("canvas_events").unwrap(),
            redraw: builder.get_object("redraw").unwrap(),
            iters: builder.get_object("iters").unwrap(),
            zoom_out: builder.get_object("btn_zoom_out").unwrap(),
            zoom_fit: builder.get_object("btn_zoom_fit").unwrap(),
            zoom_in: builder.get_object("btn_zoom_in").unwrap(),
            offset_x: 0,
            offset_y: 0,
            zoom: 0.0,
        };

        let this = Rc::new(RefCell::new(state));
        RustMandelbrotGtk::connect_signals(&this, application);
        return this;
    }

    pub fn connect_signals(this: &Rc<RefCell<Self>>, application: &gtk::Application) {
        {
            // Connect application and window, show window on app activation
            let that = this.clone();
            application.connect_activate(move |application| {
                application.add_window(&that.borrow().window);
                that.borrow().window.present();
                that.borrow_mut().redraw();
            });
        }
        // Redrawing on resize currently disabled because it doesn't work reliably
        // {
        //    // Redraw on window resize
        //     let that = this.clone();
        //     this.borrow().window.connect_size_allocate(move |_, _| {
        //         that.borrow_mut().redraw();
        //     });
        // }
        {
            // Redraw fractal on button press
            let that = this.clone();
            this.borrow().redraw.connect_clicked(move |_| {
                that.borrow_mut().redraw();
            });
        }
        {
            // Zoom in when canvas left-clicked, zoom out when right-clicked
            let that = this.clone();
            this.borrow().canvas_events.connect_button_release_event(move |_, event| {
                // If left, center or right button is clicked jump to that position
                if (1..=3).contains(&event.get_button()) {
                    let half_width = that.borrow().canvas.get_allocated_width() / 2;
                    let half_height = that.borrow().canvas.get_allocated_height() / 2;
                    let (x, y) = event.get_position();

                    that.borrow_mut().offset_x += x as isize - half_width as isize;
                    that.borrow_mut().offset_y += y as isize - half_height as isize;
                }

                // If left or right button is clicked zoom in / out
                match event.get_button() {
                    1 => that.borrow_mut().zoom += 1.0,
                    3 => that.borrow_mut().zoom -= 1.0,
                    _ => (),
                };

                // Redraw after event
                that.borrow_mut().redraw();
                gtk::Inhibit(true)
            });
        }
        {
            // Zoom out using button
            let that = this.clone();
            this.borrow().zoom_out.connect_clicked(move |_| {
                that.borrow_mut().zoom -= 1.0;
                that.borrow_mut().redraw();
            });
        }
        {
            // Zoom to original size and position using button
            let that = this.clone();
            this.borrow().zoom_fit.connect_clicked(move |_| {
                that.borrow_mut().zoom = 0.0;
                that.borrow_mut().offset_x = 0;
                that.borrow_mut().offset_y = 0;
                that.borrow_mut().redraw();
            });
        }
        {
            // Zoom in using button
            let that = this.clone();
            this.borrow().zoom_in.connect_clicked(move |_| {
                that.borrow_mut().zoom += 1.0;
                that.borrow_mut().redraw();
            });
        }
    }

    pub fn redraw(&mut self) {
        let width = self.scrolled_window.get_allocated_width() as isize - 2;
        let height = self.scrolled_window.get_allocated_height() as isize - 2;
        let max_iters = str::parse::<usize>(self.iters.get_text().as_str()).unwrap_or(10);

        let mb = mandelbrot_parallel(self.offset_x, self.offset_y, width, height, max_iters, self.zoom);
        self.canvas.draw_mandelbrot(&mb, max_iters, width as usize, height as usize);
    }
}

impl DrawMandelbrot for RustMandelbrotGtk {
    fn draw_mandelbrot(&mut self, set: &[usize], max_iters: usize, width: usize, height: usize) {
        self.canvas.draw_mandelbrot(set, max_iters, width, height);
    }
}

impl DrawMandelbrot for gtk::Image {
    fn draw_mandelbrot(&mut self, set: &[usize], max_iters: usize, width: usize, height: usize) {
        let buffer = gdk_pixbuf::Pixbuf::new(
            gdk_pixbuf::Colorspace::Rgb,
            false,
            8,
            width as i32,
            height as i32,
        ).unwrap();

        for (idx, color) in colorize(&set, max_iters).enumerate() {
            let col = (idx % width) as u32;
            let row = (idx / width) as u32;

            buffer.put_pixel(col, row, color[0], color[1], color[2], 255);
        }

        self.set_from_pixbuf(Some(&buffer));
    }
}

# GTK frontend to rust-mandelbrot
To get set-up:

1. Clone this repo
2. Ensure you have the GTK development files installed
    a. `sudo apt install libgtk-3-dev` for Ubuntu (probably)
    b. `sudo dnf install gtk3-devel` for Fedora
3. Compile and run using `cargo run --bin rust-mandelbrot-gtk --features=gtkui --release`

# rust-mandelbrot
Dynamic rendering of the Mandelbrot set in Rust and WebAssembly. 

This repository is an exercise in performant Mandelbrot and modular engineering. 
As such, it contains 4 pieces:

1. The reusable Mandelbrot library in `src/lib/`,
2. The WebAssembly front-end in `src/wasm/`: follow the instructions in `wasm/README.md`,
3. The GTK front-end in `src/uigtk/`: build and run with `cargo run --bin rust-mandelbrot-gtk --features=gtkui --release`. 
4. A CLI front-end in `src/cli/`: build and run with `cargo run --bin rust-mandelbrot-cli --features=cli --release`.
